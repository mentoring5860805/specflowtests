# BDD with SpecFlow

**Estimated reading time**: 15 min

## Story Outline
In this coding story, you will learn how to implement the BDD solution for running automated tests using SpecFlow.

## Story Organization
**Story Branch**: main
> `git checkout main`

Tags: #Selenium, #.NET, #C_Sharp, #BDD, #SpecFlow
